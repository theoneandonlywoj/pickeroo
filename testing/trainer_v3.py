
# coding: utf-8

# In[ ]:

import pandas as pd
import numpy as np
import tensorflow as tf
from environment import *
from tqdm import tqdm

env = Environment(model='small')
env.make()
env.set_obstacles_from_json()
env.initialize_obstacles()
#env.set_players([(4, 1), (4, 2)])
env.set_players_from_json()
env.initialize_players()
env.set_goals_from_json()
env.initialize_goals()

WIDTH = env.width
HEIGHT = env.height

# In[ ]:

state = env.observations
env.observations


# In[ ]:

# Deep Q Network off-policy
class DeepQNetwork:
    def __init__(
            self,
            n_actions,
            n_features,
            learning_rate=0.01,
            reward_decay=0.9,
            e_greedy=0.9,
            replace_target_iter=300,
            memory_size=50,
            batch_size=32,
            e_greedy_increment=None,
            output_graph=False,
    ):
        self.n_actions = n_actions
        self.n_features = n_features
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon_max = e_greedy
        self.replace_target_iter = replace_target_iter
        self.memory_size = memory_size
        self.batch_size = batch_size
        self.epsilon_increment = e_greedy_increment
        self.epsilon = 0.5 if e_greedy_increment is not None else self.epsilon_max

        # total learning step
        self.learn_step_counter = 0

        # initialize zero memory [s, a, r, s_]
        self.memory = np.zeros((self.memory_size, n_features * 2 + 2))

        # consist of [target_net, evaluate_net]
        self._build_net()

        t_params = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='target_net')
        e_params = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='eval_net')

        with tf.variable_scope('soft_replacement', reuse=tf.AUTO_REUSE):
            self.target_replace_op = [tf.assign(t, e) for t, e in zip(t_params, e_params)]

        self.sess = tf.Session()

        if output_graph:
            # $ tensorboard --logdir=tensorboard_logs
            tf.summary.FileWriter("./tensorboard_logs", self.sess.graph)

        self.sess.run(tf.global_variables_initializer())
        self.cost_his = []

    def _build_net(self):
        # ------------------ all inputs ------------------------
        self.s = tf.placeholder(tf.float32, [None, self.n_features], name='s')  # input State
        self.s_ = tf.placeholder(tf.float32, [None, self.n_features], name='s_')  # input Next State
        self.r = tf.placeholder(tf.float32, [None, ], name='r')  # input Reward
        self.a = tf.placeholder(tf.int32, [None, ], name='a')  # input Action

        w_initializer, b_initializer = tf.random_normal_initializer(0., 0.3), tf.constant_initializer(0.1)

        # ------------------ build evaluate_net ------------------
        with tf.variable_scope('eval_net', reuse=tf.AUTO_REUSE):
            e1 = tf.layers.dense(self.s, 100, 
                #tf.nn.tanh, 
                kernel_initializer=w_initializer,
                                 bias_initializer=b_initializer, name='e1')
            
            e1 = tf.layers.dense(e1, 100, tf.nn.relu, kernel_initializer=w_initializer,
                                 bias_initializer=b_initializer, name='e12')
            
            self.q_eval = tf.layers.dense(e1, self.n_actions, kernel_initializer=w_initializer,
                                          bias_initializer=b_initializer, name='q')

        # ------------------ build target_net ------------------
        with tf.variable_scope('target_net', reuse=tf.AUTO_REUSE):
            t1 = tf.layers.dense(self.s_, 100, 
                #tf.nn.tanh, 
                kernel_initializer=w_initializer,
                                 bias_initializer=b_initializer, name='t1')
            
            t1 = tf.layers.dense(t1, 100, tf.nn.relu, kernel_initializer=w_initializer,
                                 bias_initializer=b_initializer, name='t12')

            self.q_next = tf.layers.dense(t1, self.n_actions, kernel_initializer=w_initializer,
                                          bias_initializer=b_initializer, name='t2')

        with tf.variable_scope('q_target', reuse=tf.AUTO_REUSE):
            q_target = self.r + self.gamma * tf.reduce_max(self.q_next, axis=1, name='Qmax_s_')    # shape=(None, )
            self.q_target = tf.stop_gradient(q_target)
        with tf.variable_scope('q_eval', reuse=tf.AUTO_REUSE):
            a_indices = tf.stack([tf.range(tf.shape(self.a)[0], dtype=tf.int32), self.a], axis=1)
            self.q_eval_wrt_a = tf.gather_nd(params=self.q_eval, indices=a_indices)    # shape=(None, )
        with tf.variable_scope('loss', reuse=tf.AUTO_REUSE):
            self.loss = tf.reduce_mean(tf.squared_difference(self.q_target, self.q_eval_wrt_a, name='TD_error'))
        with tf.variable_scope('train', reuse=tf.AUTO_REUSE):
            self._train_op = tf.train.AdamOptimizer(self.lr).minimize(self.loss)

    def store_transition(self, s, a, r, s_):
        if not hasattr(self, 'memory_counter'):
            self.memory_counter = 0
        transition = np.hstack((s, [a, r], s_))
        # replace the old memory with new memory
        index = self.memory_counter % self.memory_size
        self.memory[index, :] = transition
        self.memory_counter += 1

    def choose_action(self
    	, observation):
        # to have batch dimension when feed into tf placeholder
        observation = observation[np.newaxis, :]

        if np.random.uniform() < self.epsilon:
            # forward feed the observation and get q value for every actions
            actions_value = self.sess.run(self.q_eval, feed_dict={self.s: observation})
            action = np.argmax(actions_value)
        else:
            action = np.random.randint(0, self.n_actions)
        return action

    def learn(self):
        # check to replace target parameters
        if self.learn_step_counter % self.replace_target_iter == 0:
            self.sess.run(self.target_replace_op)
            #print('\nTarget_params_replaced\n')

        # sample batch memory from all memory
        if self.memory_counter > self.memory_size:
            sample_index = np.random.choice(self.memory_size, size=self.batch_size)
        else:
            sample_index = np.random.choice(self.memory_counter, size=self.batch_size)
        batch_memory = self.memory[sample_index, :]

        _, cost = self.sess.run(
            [self._train_op, self.loss],
            feed_dict={
                self.s: batch_memory[:, :self.n_features],
                self.a: batch_memory[:, self.n_features],
                self.r: batch_memory[:, self.n_features + 1],
                self.s_: batch_memory[:, -self.n_features:],
            })

        self.cost_his.append(cost)

        # increasing epsilon
        self.epsilon = self.epsilon + self.epsilon_increment if self.epsilon < self.epsilon_max else self.epsilon_max
        self.learn_step_counter += 1

    def plot_cost(self):
        import matplotlib.pyplot as plt
        plt.plot(np.arange(len(self.cost_his)), self.cost_his)
        plt.ylabel('Cost')
        plt.xlabel('training steps')
        plt.show()


# In[ ]:

def run_RL(episodes):
    step = 0
    for episode in tqdm(range(episodes)):
        # initial observation
        print('Episode {}.'.format(episode))
        
        env.reset()
        observation = env.reset()

        while True:
            # RL choose action based on observation
            action = RL.choose_action(observation.reshape([-1, WIDTH * HEIGHT])[0])

            # RL take action and get next observation and reward
            observation_, reward, done, _ = env.step(0, action, verbose = 0)
            
            observation=observation.reshape([-1, WIDTH * HEIGHT])[0]
            observation_=observation_.reshape([-1, WIDTH * HEIGHT])[0]
                        
            RL.store_transition(observation, action, reward, observation_)

            if (step % 5 == 0):
                RL.learn()

            # swap observation
            observation = observation_
            
            env.render()

            # break while loop when end of this episode
            if done:
                print(reward)
                break
            step += 1


# In[ ]:

RL = DeepQNetwork(4, WIDTH * HEIGHT,
                  learning_rate=0.01,
                  reward_decay=0.9,
                  e_greedy=0.5,
                  e_greedy_increment = 0.01, 
                  replace_target_iter = 1,
                  memory_size=1,
                  output_graph=True
                      )


# In[ ]:

run_RL(100000)


# In[ ]:

RL.plot_cost()

