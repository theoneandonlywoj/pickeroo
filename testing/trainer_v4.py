
# coding: utf-8

# In[1]:

import tensorflow as tf
import numpy as np
from tqdm import tqdm
from environment import *



# In[2]:


VERBOSE = 0


# In[3]:

env = Environment(model='v1.0')
env.make()
env.set_obstacles_from_json()
env.initialize_obstacles()
env.set_players_from_json()
env.initialize_players()
env.set_goals_from_json()
env.initialize_goals()

WIDTH = env.width
HEIGHT = env.height
# In[4]:

print(env.observations)


# In[5]:

env.reset()


# In[6]:

n_actions = 4
n_epochs = 5000
n = 0
average = []
step = 1
batch_size = 500
render = False

# Define our three actions of moving forward, turning left & turning right
choice = ['UP',
          'RIGHT',
          'DOWN',
          'LEFT'
         ]


# In[7]:




# In[8]:

# Hyper Parameters
alpha = 0.01
gamma = 0.99
normalize_r = True
save_path='models/pickeroo.ckpt'
value_scale = 0.5
entropy_scale = 0.00
gradient_clip = 40


# In[9]:

# Apply discount to episode rewards & normalize
def discount(r, gamma, normal):
    discount = np.zeros_like(r)
    G = 0.0
    for i in reversed(range(0, len(r))):
        G = G * gamma + r[i]
        discount[i] = G
    # Normalize 
    if normal:
        mean = np.mean(discount)
        std = np.std(discount)
        discount = (discount - mean) / (std)
    return discount


# In[10]:

# Conv Layers
convs = [16, 16]
kerns = [5, 5]
strides = [1, 1]
pads = 'valid'
fc = 128
activ = tf.nn.relu


# In[11]:

# Tensorflow Variables
X = tf.placeholder(tf.float32, (None, WIDTH, HEIGHT,1), name='X')
Y = tf.placeholder(tf.int32, (None,), name='actions')
R = tf.placeholder(tf.float32, (None,), name='reward')
N = tf.placeholder(tf.float32, (None), name='episodes')
D_R = tf.placeholder(tf.float32, (None,), name='discounted_reward')

w_initializer, b_initializer = tf.random_normal_initializer(0., 0.3), tf.constant_initializer(0.1)
# In[12]:

# Policy Network
conv1 = tf.layers.conv2d(
        inputs = X,
        filters = convs[0],
        kernel_size = kerns[0],
        strides = strides[0],
        padding = pads,
        #activation = activ,
        kernel_initializer=w_initializer,
        bias_initializer=b_initializer,
        name='conv1')

conv2 = tf.layers.conv2d(
        inputs=conv1,
        filters = convs[1],
        kernel_size = kerns[1],
        strides = strides[1],
        padding = pads,
        #activation = activ,
        kernel_initializer=w_initializer,
        bias_initializer=b_initializer,
        name='conv2')

flat = tf.layers.flatten(conv2)

dense = tf.layers.dense(
        inputs = flat, 
        units = fc, 
        #activation = activ,
        kernel_initializer=w_initializer,
        bias_initializer=b_initializer,
        name = 'fc')

logits = tf.layers.dense(
         inputs = dense, 
         units = n_actions, 
         name='logits')

value = tf.layers.dense(
        inputs=dense, 
        units = 1, 
        name='value')

calc_action = tf.multinomial(logits, 1)
aprob = tf.nn.softmax(logits)
action_logprob = tf.nn.log_softmax(logits)


# In[13]:

tf.trainable_variables()


# In[14]:

def rollout(batch_size, render):
    
    states, actions, rewards, rewardsFeed, discountedRewards = [], [], [], [], []
    #state = resize(env.reset())
    state = env.reset()
    env.reset()
    episode_num = 0 
    action_repeat = 1
    reward = 0
    
    while True: 
        
        
        # Run State Through Policy & Calculate Action
        feed = {X: state.reshape(1, WIDTH, HEIGHT, 1)}
        action = sess.run(calc_action, feed_dict=feed)
        action = action[0][0]
        
        # Perform Action
        for i in range(action_repeat):
            state2, reward2, done, info = env.step(player_id = 0, action_id = choice[action], verbose = VERBOSE)
            reward += reward2
            if done:
                break
        
        # Store Results
        states.append(state)
        rewards.append(reward)
        actions.append(action)
        
        # Update Current State
        reward = 0
        state = state2
        
        if done:
            # Track Discounted Rewards
            rewardsFeed.append(rewards)
            discountedRewards.append(discount(rewards, gamma, normalize_r))
            
            if len(np.concatenate(rewardsFeed)) > batch_size:
                break
                
            # Reset Environment
            rewards = []
            state = env.reset()
            env.reset()
            episode_num += 1
                         
    return np.stack(states), np.stack(actions), np.concatenate(rewardsFeed), np.concatenate(discountedRewards), episode_num


# In[15]:

mean_reward = tf.divide(tf.reduce_sum(R), N)

# Define Losses
pg_loss = tf.reduce_mean((D_R - value) * tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits, labels=Y))
value_loss = value_scale * tf.reduce_mean(tf.square(D_R - value))
entropy_loss = -entropy_scale * tf.reduce_sum(aprob * tf.exp(aprob))
loss = pg_loss + value_loss - entropy_loss

# Create Optimizer
optimizer = tf.train.AdamOptimizer(alpha)
grads = tf.gradients(loss, tf.trainable_variables())
grads, _ = tf.clip_by_global_norm(grads, gradient_clip) # gradient clipping
grads_and_vars = list(zip(grads, tf.trainable_variables()))
train_op = optimizer.apply_gradients(grads_and_vars)

# Initialize Session
sess = tf.Session()
init = tf.global_variables_initializer()
sess.run(init)


# In[16]:

# Setup TensorBoard Writer
writer = tf.summary.FileWriter("./tensorboard_logs/trainer_v4")
tf.summary.scalar('Total_Loss', loss)
tf.summary.scalar('PG_Loss', pg_loss)
tf.summary.scalar('Entropy_Loss', entropy_loss)
tf.summary.scalar('Value_Loss', value_loss)
tf.summary.scalar('Reward_Mean', mean_reward)
tf.summary.histogram('Conv1', tf.trainable_variables()[0])
tf.summary.histogram('Conv2', tf.trainable_variables()[2])
tf.summary.histogram('FC', tf.trainable_variables()[4])
tf.summary.histogram('Logits', tf.trainable_variables()[6])
tf.summary.histogram('Value', tf.trainable_variables()[8])
write_op = tf.summary.merge_all()


# In[ ]:

# Load model if exists
saver = tf.train.Saver(tf.global_variables())
load_was_success = True 
try:
    save_dir = '/'.join(save_path.split('/')[:-1])
    ckpt = tf.train.get_checkpoint_state(save_dir)
    load_path = ckpt.model_checkpoint_path
    saver.restore(sess, load_path)
except:
    print("No saved model to load. Starting new session")
    writer.add_graph(sess.graph)
    load_was_success = False
else:
    print("Loaded Model: {}".format(load_path))
    saver = tf.train.Saver(tf.global_variables())
    step = int(load_path.split('-')[-1])+1


# In[ ]:

for step in tqdm(range(n_epochs+1)):
    # Gather Training Data
    print('Epoch', step)
    s, a, r, d_r, n = rollout(batch_size,render)
    mean_reward = np.sum(r)/n
    average.append(mean_reward)
    print('Training Episodes: {}  Average Reward: {:4.2f}  Total Average: {:4.2f}'.format(n, mean_reward, np.mean(average)))
          
    # Update Network
    sess.run(train_op, feed_dict={X:s.reshape(len(s), WIDTH, HEIGHT, 1), Y:a, D_R: d_r})
          
    # Write TF Summaries
    summary = sess.run(write_op, feed_dict={X:s.reshape(len(s), WIDTH, HEIGHT, 1), Y:a, D_R: d_r, R: r, N:n})
    writer.add_summary(summary, step)
    writer.flush()
          
    # Save Model
    if step % 10 == 0:
          print("SAVED MODEL")
          saver.save(sess, save_path, global_step=step)
          
    step += 1


# In[ ]:



