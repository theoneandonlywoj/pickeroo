
# coding: utf-8

# In[ ]:


from environment import *
from tqdm import tqdm
import time


# In[ ]:


WIDTH = 7
HEIGHT = 20


# In[ ]:


env = Environment(model='v1.0')


# In[ ]:


env.make()


# In[ ]:


env.set_obstacles_from_json()


# In[ ]:


env.initialize_obstacles()


# In[ ]:


env.set_players_from_json()


# In[ ]:


env.initialize_players()


# In[ ]:


env.players


# In[ ]:


env.set_goals_from_json()


# In[ ]:


env.initialize_goals()


# In[ ]:


obs, reward, done, info = env.step(player_id = 0, action_id = 1)


# In[ ]:


'''for i in tqdm(range(100000)):
    random_action = np.random.choice(4) 
    obs, reward, done, info = env.step(player_id = 0, action_id = random_action, verbose = 0)
    #random_action = np.random.choice(4)
    #obs, reward, done, info = env.step(player_id = 1, action_id = random_action, verbose = 0)'''


# In[ ]:


env.reset()

plt.ion()
for _ in range(100):
    random_action = np.random.choice(4)
    obs, reward, done, info = env.step(player_id=0, 
    	action_id=random_action, 
    	verbose=3)
    env.render()
    if done:
        env.reset()
    time.sleep(0.1)
