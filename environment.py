import numpy as np
import matplotlib.pyplot as plt
import json

class Environment:
    def __init__(self):
        ''' Get parameters of the environment '''
        self.done = False
        self.allowed_actions = ["UP", "RIGHT", "DOWN", "LEFT"]
        self.action_space_n = len(self.allowed_actions)
        self.rewards = []
        
        self.initialise_players()
        self.initialise_obstacles()
        self.initialise_goals()

    def action_space(self):
        return self.allowed_actions

    def make(self):
        self.width = 5
        self.height = 6
        
        self.players_bump_reward = -0.3
        self.goals_reward = 0.5
        self.step_reward = -0.1
        self.obstalce_reward = -0.2
        
        self.init_grid = self.step_reward * np.ones(shape=(self.height, self.width),
                                                    dtype=np.float32)
        self.calculate_grid()
        
    def calculate_grid(self):
        self.observations = np.copy(self.init_grid)
        
        self.set_players()
        self.set_obstacles()
        self.set_goals()
        
    def reset(self):
        self.initialise_players()
        self.initialise_obstacles()
        self.initialise_goals()
        
        self.calculate_grid()
        
        self.rewards = []
        self.done = False
        
        return np.copy(self.observations)
    
    def initialise_players(self):
        self.players = [(2, 1)]
        
    def initialise_goals(self):
        self.goals = [(2, 2), (2, 3)]
        
    def initialise_obstacles(self):
        self.obstacles = [(3, 3), (3, 4)]
    
    def set_players(self):
        for player in self.players:
            self.observations[player] = self.players_bump_reward

    def set_goals(self):
        for goal in self.goals:
            self.observations[goal] = self.goals_reward
            
    def set_obstacles(self):
        for obstacle in self.obstacles:
            self.observations[obstacle] = self.obstalce_reward
            
    def step(self, action, player_id=0):
        player_location = self.players[player_id]
        new_position = self.calculate_player_next_position(player_location, action)
        
        new_position_checked, step_reward = self.detect_collision(player_location, new_position)
        self.players[player_id] = new_position_checked
        self.calculate_grid()
        self.rewards.append(step_reward)
        
        self.check_if_done()
        
        return np.copy(self.observations), \
               np.sum(self.rewards), \
               self.done, \
               "Info not implemented"
        # return np.copy(self.observations)
    
    def check_if_done(self, rule='only_all_goal_gathered'):
        if rule == 'only_all_goal_gathered':
            if len(self.goals) == 0:
                self.done = True
     
    def detect_collision(self, old_position, new_position):
        if new_position in self.obstacles:
            return old_position, self.obstalce_reward
        elif new_position in self.goals:
            self.goals = [goal for goal in self.goals if goal != new_position]
            return new_position, self.goals_reward
        elif new_position in self.players:
            return old_position, self.players_bump_reward
        # If the player attempts to go off the grid
        elif new_position[0] < 0 \
            or new_position[1] < 0 \
            or new_position[0] > (self.height - 1) \
            or new_position[1] > (self.width - 1):
            return old_position, self.obstalce_reward
        else: 
            return new_position, self.step_reward
            
    def calculate_player_next_position(self, current_position, action):
        a = current_position[0]
        b = current_position[1]
        if action == 'UP':
            new_position = (a + 1, b)
        elif action == 'DOWN':
            new_position = (a - 1, b)
        elif action == 'LEFT':
            new_position = (a, b - 1)
        elif action == 'RIGHT':
            new_position = (a, b + 1)
        return new_position
    
    def render(self):
        plt.ion()
        if not plt.get_fignums():
            self.fig = plt.figure(figsize=(10, 10))
        plt.pcolor(np.copy(self.observations),
                           cmap=plt.cm.coolwarm_r, vmin=-1, vmax=1)
        plt.colorbar()
        plt.draw()
        plt.pause(0.0001)
        self.fig.canvas.flush_events()
        plt.clf()
        