import numpy as np
import matplotlib.pyplot as plt
import json

class Environment:
	''' Environment class representing warehouse environment.'''
	def __init__(self, model):
		'''Get parameters of the environment
		'''
		self.model = model
		self.player_position_punishment_value = -0.3
		self.obstacle_position_punishment_value = -1
		self.goal_reward_value = 10
		self.step_punishment_value = -0.1
		self.done = False
		self.action_space_n = 4
				
	def make(self):
		'''Create'''
		file_location = './environment_setting/JSON/' + self.model + '/layout.json'
		with open(file_location) as f:
		    layout_dict = json.load(f)
		self.width = layout_dict['width']
		self.height = layout_dict['height']
		self.observations = self.step_punishment_value * np.ones(shape=(self.height, self.width), dtype=np.float32)
		self.obstacles = []

	def set_players_from_json(self):
		file_location = './environment_setting/JSON/' + self.model + '/layout.json'
		with open(file_location) as f:
		    layout_dict = json.load(f)
		list_of_player_array = layout_dict['players']
		self.players_to_be_initialized = list_of_player_array
		self.players_last_position_punishment_value = np.ones(shape=(1, len(list_of_player_array))) * self.step_punishment_value
	'''
	DEPRECATED		
	def set_obstacles(self, list_of_obstacle_tuples):
		self.obstacles_to_be_initialized = list_of_obstacle_tuples
	'''
	def set_obstacles_from_json(self):
		file_location = './environment_setting/JSON/' + self.model + '/layout.json'
		with open(file_location) as f:
		    layout_dict = json.load(f)
		self.obstacles_to_be_initialized = layout_dict['obstacles']
	'''
	def set_goals(self, list_of_goals):
		self.goals_to_be_initialized = list_of_goals
	'''
	def set_goals_from_json(self):
		file_location = './environment_setting/JSON/' + self.model + '/orders.json'
		with open(file_location) as f:
		    orders_dict = json.load(f)
		self.goals_to_be_initialized = orders_dict['goals']

	def initialize_players(self):
		''' Create players
		Arguments:
			players - array of tuples with the initial locations of the players(y, x)
		'''
		self.players = []
		self.rewards = []
		for player in self.players_to_be_initialized:
			player_position_x = player[0]
			player_position_y = player[1]
			# Update the grid with value for the player
			self.observations[player_position_y, player_position_x] = self.player_position_punishment_value 
			self.players.append(player)
			self.rewards.append(0)
	
	def initialize_obstacles(self):
		for obstacle in self.obstacles_to_be_initialized:
			obstacle_x = obstacle[0]
			obstacle_y = obstacle[1]

			self.observations[obstacle_y, obstacle_x] = self.obstacle_position_punishment_value
			self.obstacles.append((obstacle_x, obstacle_y))
	
	def initialize_goals(self):
		self.goals=[]
		self.goals_grid_initial_values = []
		self.goals_picked_up = []
		for goal in self.goals_to_be_initialized:
			goal_x = goal[0]
			goal_y = goal[1]

			self.goals_grid_initial_values.append(self.observations[goal_y, goal_x])
			self.observations[goal_y, goal_x] = self.goal_reward_value
			self.goals.append((goal_x, goal_y))

	def reset(self):
		self.make()
		self.initialize_obstacles()
		self.initialize_players()
		self.initialize_goals()
		self.done = False
		return self.observations
		

	def action_space(self):
		print("Discrete:")
		print("[UP, RIGHT, DOWN, LEFT]")
		print("[0, 1, 2, 3]")
		return np.array([0, 1, 2, 3])   
			
	def step(self, player_id, action_id, verbose=0):
		'''
		Actions:
			0 - Go up
			1 - Go right
			2 - Go down
			3 - Go left
		'''
		# If all pick ups are Done then do nothing.
		if self.done == True:
			info = 'Done'
		else:

			player_position_x = self.players[player_id][0]
			player_position_y = self.players[player_id][1]
			
			
			if (action_id == 0 or action_id == 'UP'):
				new_player_position_x = player_position_x
				new_player_position_y = player_position_y + 1
				action_id = 'UP'
			
			if (action_id == 1 or action_id == 'RIGHT'):
				new_player_position_x = player_position_x + 1
				new_player_position_y = player_position_y
				action_id = 'RIGHT'
				
			if (action_id == 2 or action_id == 'DOWN'):
				new_player_position_x = player_position_x
				new_player_position_y = player_position_y - 1 
				action_id = 'DOWN'
				
			if (action_id == 3 or action_id == 'LEFT'):
				new_player_position_x = player_position_x - 1
				new_player_position_y = player_position_y
				action_id = 'LEFT'
			
			info = 'Player {}, made action {}.'.format(player_id, action_id)   
				  
			# Going out of boundy of the grid check
			if ((new_player_position_x < 0) or (new_player_position_y < 0) or (new_player_position_x == self.width) or (new_player_position_y == self.height)):
				self.rewards[player_id] += self.obstacle_position_punishment_value
				new_player_position_x = player_position_x
				new_player_position_y = player_position_y
				self.done = True
				info += " Illegal action: out of boundary."
			else:
				# Add loss function as if the player made the move
				self.rewards[player_id] += self.observations[new_player_position_y, new_player_position_x]
					
				# Add check if the player hits the obstacle
				if(tuple([new_player_position_x, new_player_position_y]) in self.obstacles):
					new_player_position_x = player_position_x
					new_player_position_y = player_position_y
					info += " Illegal action: obstacle."
					self.done = True

				# Checking collisions with other players
				players_collisions_to_check = [player for player in self.players if player != (player_position_x, player_position_y)]
				  
				if(tuple([new_player_position_x, new_player_position_y]) in players_collisions_to_check):
					new_player_position_x = player_position_x
					new_player_position_y = player_position_y
					info += " Illegal action: another player."  
					self.done = True
				
				
			# Replace punishment value of the player with previous value
			# If it is the goal -> replace by its initial value / step value
			# Otherwise, replace by previous value for the location
			if(tuple([player_position_x, player_position_y]) in self.goals):
				self.observations[player_position_y, player_position_x] = self.step_punishment_value
			else:
				self.observations[player_position_y, player_position_x] = self.players_last_position_punishment_value[0][player_id]

			# If the new position is a goal, then append it to the list of the rewards
			if((tuple([new_player_position_x, new_player_position_y]) in self.goals) and 
				(tuple([new_player_position_x, new_player_position_y]) not in self.goals_picked_up)):
				self.goals_picked_up.append(tuple([new_player_position_x, new_player_position_y]))
				
			# Check if all pick ups are done:
			if(len(self.goals) == len(self.goals_picked_up)):
				self.done = True

			# Assign new punishment value 
			self.players_last_position_punishment_value[0][player_id] = self.observations[new_player_position_y, new_player_position_x]
			
			# Make a move to the new location
			self.players[player_id] = (new_player_position_x, new_player_position_y)

			# Replace the value for the new position
			self.observations[new_player_position_y, new_player_position_x] = self.player_position_punishment_value 
			   
				
		if(verbose == 1):
			print(self.rewards)
		elif(verbose == 2):
			print('-' * 70)
			print(self.observations)
		elif(verbose == 3):
			print('-' * 70)
			print('Observation:')
			print(self.observations)
			print('Rewards:')
			print(self.rewards)
			print('Total Reward:')
			print(sum(self.rewards))
			print('Done:')
			print(self.done)
			print('Info:')
			print(info)
		else:
			pass
			
		return self.observations, np.sum(self.rewards), self.done, info

	def render(self):
		plt.ion()
		if not plt.get_fignums():
			self.fig = plt.figure(figsize=(10, 10))
		plt.pcolor(np.copy(self.observations), cmap=plt.cm.coolwarm_r, vmin=-1, vmax=1)
		plt.colorbar()
		plt.draw()
		plt.pause(0.0001)
		self.fig.canvas.flush_events()
		plt.clf()